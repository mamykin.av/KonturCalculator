package ru.mamykin.konturcalc;

/**
 * Creation date: 7/8/2017
 * Creation time: 6:37 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class Expression {
    private String expression;
    private Operation operation;
    private Double operand;

    public Expression() {
    }

    public Expression(String expression) {
        this.expression = expression;
    }

    public Expression(Operation operation) {
        this.operation = operation;
    }

    public Expression(Double operand) {
        this.operand = operand;
    }

    public String getExpression() {
        return expression.substring(1, expression.length() - 1);
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Double getOperand() {
        return operand;
    }

    public void setOperand(Double operand) {
        this.operand = operand;
    }

    public boolean isExpression() {
        return expression != null;
    }

    public boolean isOperation() {
        return operation != null;
    }

    public boolean isOperand() {
        return operand != null;
    }
}