package ru.mamykin.konturcalc;

/**
 * Creation date: 7/10/2017
 * Creation time: 7:31 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class Operation {
    private IOperation operation;

    public Operation(IOperation operation) {
        this.operation = operation;
    }

    public double calculate(double a, double b) {
        return operation.calculate(a, b);
    }

    public int getPriority() {
        return operation.getPriority();
    }

    public static Operation parseOperation(String operationStr) {
        switch (operationStr) {
            case "+":
                return new Operation(new PlusOperation());
            case "-":
                return new Operation(new MinusOperation());
            case "/":
                return new Operation(new DivideOperation());
            default:
                return new Operation(new MultiplyOperation());
        }
    }

    @Override
    public String toString() {
        return operation.toString();
    }

    interface IOperation {
        double calculate(double a, double b);

        int getPriority();

        String toString();
    }

    static class PlusOperation implements IOperation {
        @Override
        public double calculate(double a, double b) {
            return a + b;
        }

        @Override
        public int getPriority() {
            return 1;
        }

        @Override
        public String toString() {
            return "+";
        }
    }

    static class MinusOperation implements IOperation {
        @Override
        public double calculate(double a, double b) {
            return a - b;
        }

        @Override
        public int getPriority() {
            return 1;
        }

        @Override
        public String toString() {
            return "-";
        }
    }

    static class DivideOperation implements IOperation {
        @Override
        public double calculate(double a, double b) {
            return a / b;
        }

        @Override
        public int getPriority() {
            return 2;
        }

        @Override
        public String toString() {
            return "/";
        }
    }

    static class MultiplyOperation implements IOperation {
        @Override
        public double calculate(double a, double b) {
            return a * b;
        }

        @Override
        public int getPriority() {
            return 2;
        }

        @Override
        public String toString() {
            return "*";
        }
    }
}