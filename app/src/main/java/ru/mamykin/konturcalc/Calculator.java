package ru.mamykin.konturcalc;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Creation date: 7/8/2017
 * Creation time: 3:48 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class Calculator {
    private static final int PRIORITY_HIGH = 2;
    private static final int PRIORITY_LOW = 1;

    private final Pattern operandPattern = Pattern.compile("^-?\\d+\\.?\\d*");
    private final Pattern operationPattern = Pattern.compile("^[+\\-*/]");

    public boolean checkValid(final String expression) {
        if (TextUtils.isEmpty(expression))
            return false;

        Pattern expressPattern = Pattern.compile("[0-9/ +\\-().*]+");
        Matcher matcher = expressPattern.matcher(expression);

        return matcher.matches();
    }

    private List<Expression> parseExpressions(String rawExpression) {
        final List<Expression> expressions = new ArrayList<>();
        // Регулярные выражения лучше переписать на ручную проверку, будет работать быстрее
        Expression parsedExpression = null;
        while (rawExpression.length() != 0) {
            Matcher operandMatcher = operandPattern.matcher(rawExpression);
            Matcher operationMatcher = operationPattern.matcher(rawExpression);
            if ((parsedExpression == null || !parsedExpression.isOperand())
                    && operandMatcher.lookingAt()) {
                // Операнд
                final String operand = operandMatcher.group();
                rawExpression = rawExpression.substring(operand.length(), rawExpression.length());
                parsedExpression = new Expression(Double.parseDouble(operand));
            } else if ((parsedExpression == null || !parsedExpression.isOperand())
                    && hasNestedExpression(rawExpression)) {
                // Выражение
                final String expression = getNestedExpression(rawExpression);
                rawExpression = rawExpression.substring(expression.length(), rawExpression.length());
                parsedExpression = new Expression(expression);
            } else if (operationMatcher.lookingAt()) {
                // Операция
                final String operation = operationMatcher.group();
                rawExpression = rawExpression.substring(operation.length(), rawExpression.length());
                parsedExpression = new Expression(Operation.parseOperation(operation));
            }
            expressions.add(parsedExpression);
        }
        return expressions;
    }

    private boolean hasNestedExpression(String rawExpression) {
        return rawExpression.startsWith("(");
    }

    private String getNestedExpression(String rawExpression) {
        // Получаем вложенные скобки из строки, т.к. Java не поддерживает рекурсивные регулярки
        int deep = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rawExpression.length(); i++) {
            if (rawExpression.substring(i, i + 1).equals("(")) {
                deep++;
            } else if (rawExpression.substring(i, i + 1).equals(")")) {
                deep--;
            }
            sb.append(rawExpression.substring(i, i + 1));
            if (deep == 0) {
                break;
            }
        }
        return sb.toString();
    }

    private void calculateInnerExpressions(List<Expression> expressions) {
        // Проверяем, есть ли вложенные выражения
        for (int i = 0; i < expressions.size(); i++) {
            Expression expression = expressions.get(i);
            if (expression.isExpression()) {
                // Вычисляем рекурсивно
                double operand = calculate(expression.getExpression());
                Expression operandExpr = new Expression();
                operandExpr.setOperand(operand);
                expressions.set(i, operandExpr);
            }
        }
    }

    private void calculateOperations(int priority, List<Expression> expressions) {
        for (int i = 0; i < expressions.size(); i++) {
            Expression expression = expressions.get(i);
            if (expression.isOperation() && expression.getOperation().getPriority() == priority) {
                // Берём операнд слева и справа, вычисляем и заменяем {3,+,2} на {5}
                double op1 = expressions.get(i - 1).getOperand();
                double op2 = expressions.get(i + 1).getOperand();
                double result;
                Operation operation = expression.getOperation();
                result = operation.calculate(op1, op2);
                expressions.set(i - 1, new Expression(result));
                expressions.remove(i);
                expressions.remove(i);
                i = 0;
            }
        }
    }

    public double calculate(String rawExpression) throws RuntimeException {
        List<Expression> expressions = parseExpressions(rawExpression);

        calculateInnerExpressions(expressions);

        calculateOperations(PRIORITY_HIGH, expressions);
        calculateOperations(PRIORITY_LOW, expressions);

        return expressions.get(0).getOperand();
    }

    public String calculateExpression(String rawExpression) {
        try {
            double result = calculate(rawExpression);
            if (result % 1 == 0)
                return String.valueOf((int) result);
            return String.valueOf(result);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}