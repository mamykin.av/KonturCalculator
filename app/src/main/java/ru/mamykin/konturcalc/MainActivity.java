package ru.mamykin.konturcalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvResult;
    private EditText etExpression;
    private Calculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnCalculate).setOnClickListener(this);
        findViewById(R.id.btnClear).setOnClickListener(this);
        tvResult = (TextView) findViewById(R.id.tvResult);
        etExpression = (EditText) findViewById(R.id.etExpression);
        calculator = new Calculator();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClear:
                tvResult.setText(null);
                etExpression.getText().clear();
                break;
            case R.id.btnCalculate:
                final String expression = etExpression.getText().toString();
                if (!calculator.checkValid(expression)) {
                    etExpression.setError(getString(R.string.main_expression_error));
                    return;
                }
                String result = calculator.calculateExpression(expression);
                if (result == null) {
                    etExpression.setError(getString(R.string.main_expression_error));
                    return;
                }
                tvResult.setText(getString(R.string.main_result_format, result));
                break;
        }
    }
}